
<?php  

	ini_set("display_erros",1);
	ini_set("display_startup_erros",1);
	error_reporting(E_ALL);
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Update</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <?php
    include 'config.php';
    if(!isset($_POST["atualizar"]))
    {
      
      $id=$_GET['id'];
      $nome=$_GET["nome"];
      $sobrenome=$_GET["sobrenome"];
      $email=$_GET["email"];
    }
    else 
    {
        $id=$_GET['id'];
        $nome=$_POST["nome"];
        $sobrenome=$_POST["sobrenome"];
        $email=$_POST["email"];
        $sql="UPDATE tb_usuarios SET nome=:nome, sobrenome=:sobrenome, email=:email WHERE id=:id";
        $resultado=$base->prepare($sql);
        $resultado->bindParam(':nome', $nome);
		$resultado->bindParam(':sobrenome', $sobrenome);
		$resultado->bindParam(':email', $email);
		$resultado->bindParam(':id', $id);

        $resultado->execute();
        
       header("Location:index.php");
    }  
  ?>
	<form name="form1" method="POST" action="<?php $_SERVER['PHP_SELF']?>">
	<table width="45%" border="1" align="center">


		<tr>
			<td></td>
			<td><label for="id"></label>	
			<input type="hidden" name="id" id="id"></td>
		</tr>
		<tr>
			<td>Nome</td>
			<td> <label for="nome"></label>
			<input type="text" name="nome" id="nome" value="<?php echo $nome?>"></td>		
		</tr>
		<tr>
			<td>Sobrenome</td>
			<td> <label for="sobrenome"></label>
			<input type="text" name="sobrenome" id="sobrenome" value="<?php echo $sobrenome?>"></td>		
		</tr>
		<tr>
			<td>Email</td>
			<td> <label for="email"></label>
			<input type="email" name="email" id="email" value="<?php echo $email?>"></td>		
		</tr>
		<tr>
			<td colspan="2"> <input type="submit" name="atualizar" id="atualizar" value="Atualizar"></td>

		</tr>
			
	</table>
	</form>

	<p>&nbsp;</p>
</body>
</html>